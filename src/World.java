import java.util.ArrayList;
import java.util.List;
import processing.core.PVector;

public class World {

  MySketch sketch;
  private List<Metaball> metaballs;
  private int width;
  private int height;

  public World(MySketch sketch, int height, int width) {
    this.sketch = sketch;
    this.height = height;
    this.width = width;
    this.metaballs = new ArrayList<>();

    createMetaballs();
  }

  public void update() {
    for (Metaball metaball : metaballs) {
      metaball.update(width, height);
    }
  }

  public void draw() {
    sketch.background(255);

    float boundary = 1F;

    for (int i = 0; i < width; i++) {
      for (int j = 0; j < height; j++) {
        double f_ij = explicitFunction(i, j);

        if (f_ij > boundary) {
          sketch.point(i, j);
        }
      }
    }
  }

  private float forceMagnitudeSum(PVector position) {
    float c = 20;

    float sum = 0;

    for (Metaball metaball : metaballs) {
      PVector metaballPosition = metaball.getPosition();

      float dx = metaballPosition.x - position.x;
      float dy = metaballPosition.y - position.y;
      float distance = (float) Math.sqrt(dx * dx + dy * dy);

      sum += c * metaball.getSize() / (distance * distance);
    }

    return sum;
  }

  private float explicitFunction(float x, float y) {
    return forceMagnitudeSum(new PVector(x, y));
  }

  private float explicitFunction(int x, int y) {
    return explicitFunction((float) x, (float) y);
  }

  private void createMetaballs() {
    for (int i = 0; i < 5; i++) {
      metaballs.add(Metaball.generateRandom(width, height, 5, 250));
    }
  }
}






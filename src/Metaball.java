import java.util.Random;
import processing.core.PVector;

public class Metaball {
  private PVector position;
  private PVector velocity;

  private float size;

  private static Random random = new Random(1);

  public static Metaball generateRandom(int width, int height, float max_vel, float max_size) {
    PVector position = new PVector(Metaball.random.nextInt(width), Metaball.random.nextInt(height));
    PVector velocity = new PVector(Metaball.random.nextFloat() * max_vel, Metaball.random.nextFloat() * max_vel);

    float size = Metaball.random.nextFloat() * max_size;

    System.out.println(position);
    System.out.println(velocity);
    System.out.println(size);

    return new Metaball(position, velocity, size);
  }

  public PVector getPosition() {
    return position;
  }

  public float getSize() {
    return size;
  }

  public Metaball(PVector position, PVector velocity, float size) {
    this.position = position;
    this.velocity = velocity;
    this.size = size;
  }

  public void update(int maxX, int maxY) {
    this.position = PVector.add(this.position, this.velocity);

    rectBoundCollisions(maxX, maxY);
  }

  private void rectBoundCollisions(int maxX, int maxY) {
    if (this.position.x < 0) {
      this.position.x = -this.position.x;
      this.velocity.x = -this.velocity.x;
    } else if (this.position.y < 0) {
      this.position.y = -this.position.y;
      this.velocity.y = -this.velocity.y;
    } else if (this.position.x > maxX) {
      this.position.x = 2 * maxX - this.position.x;
      this.velocity.x = -this.velocity.x;
    } else if (this.position.y > maxY) {
      this.position.y = 2 * maxY - this.position.y;
      this.velocity.y = -this.velocity.y;
    }
  }
}
